@echo off
REM call emcc lib/demo.c -s WASM=1 -s EXPORTED_FUNCTIONS="['_getNum', '_main', '_greet']" -o public/demo.js
REM call emcc lib/imports.c -s WASM=1 -s EXPORTED_FUNCTIONS="['_main']" -o public/demo.js
REM call emcc lib/strings.c -s WASM=1 -s EXPORTED_FUNCTIONS="['_getStr']" -o public/demo.js
REM call emcc lib/strings.c -s WASM=1 -s EXPORTED_FUNCTIONS="['_getStr', '_getNumber']" -s TOTAL_MEMORY=64MB -o public/demo.js
REM call emcc lib/strings.c -s WASM=1 -s EXPORTED_FUNCTIONS="['_getStr', '_getNumber']" -o public/demo.js
REM call emcc lib/runtime.c -s WASM=1 -s EXPORTED_FUNCTIONS="['_getStr']" -o public/demo.js
REM call emcc lib/runtime.c -s WASM=1 --emrun -o public/index.html
call emcc lib/canvas.c -s WASM=1 -s EXPORTED_FUNCTIONS="['_main','_getCircles']" -o public/demo.js