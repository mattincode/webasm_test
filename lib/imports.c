#include <stdio.h>
#include <emscripten.h>

int main() {
  printf("WASM Ready from C!\n");
  emscripten_run_script("console.log('Hello from C!!!')");  
  return 1;
}
