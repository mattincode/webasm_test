# WebAsm demo project

Project to try out webassembly on personal development time.

## Project setup

0. Install Empscripten, see "Emscripten" below!
1. Get project: **git clone git@gitlab.com:mattincode/webasm_test.git**
2. Enter webasm_test-folder and run **npm install**
3. Run **shell.bat** on commandline (this will call the empscripten.bat file before launching VS Code setting up env-variables)
4. Start the project using **node server.js**

## Goals

- Setup Emscripten dev-environment (can use https://wasdk.github.io/WasmFiddle/ but this is slow going dev in the long run + misses all the type-cohersion that Emscripten provide)
- Setup project (c-code and html/js)
- Use WasmFiddle for first example and access c-function from JS (index_old.html + main.c).
- Add example of accessing JS from C.
- Add example of string handling using native Wasm (non Emscripten)
- Add example of using Emscripten string handling
- Test emscripten_run_script to execute JS from C.
- Benchmark example doing simple prime number calculations.
- Write bubbles-example(canvas.c) where most of the work is done in C-code.
  - The initial render is done after wasm is loaded etc. by calling EM_ASM(render)
  - The render-method then calls requestAnimationFrame where all bubble-logic is inside the C-code.
  - JS in this case is used to draw on the canvas.

## Emscripten

### Install Emscripten

- Install on Windows WSL, install 64-bit cmake and then emscripten from https://emscripten.org/
- git clone emsdk
- emsdk install
- emsdk activate latest
- May need to edit emscripten-config(c:\Users\mandersson\.emscripten) and set path to node: NODE_JS='c:/Program Files/nodejs/node.exe'
- Make env-variables permanent by running emsdk_env.bat
  - created a emscripten-bat file that sets this up!

### Compiling

- emcc lib/demo.c -s WASM=1 -o public/demo.js
- By changing output file extenstion to .html a web page can be output.
- Wasm only output by using: emcc lib/demo.c -s WASM=1 -s SIDE_MODULE=1 -o public/demo.wasm
- Add optimizations by: emcc lib/demo.c -s WASM=1 -O2 -o public/demo.js
- Append other javascript: emcc lib/demo.c -s WASM=1 --post-js public/other.js -o public/demo.js
- Add list of exported functions: emcc lib/demo.c -s WASM=1 -s EXPORTED_FUNCTIONS="['_getNum', '_main']" -o public/demo.js

**Above will create a wasm and some glue code to enable us to just include the glue js-file**

### Emscripten features

- printf will automatically be translated by emscripten.
- Exported C-functions (getNum in this case) can be called directly from JS with \_getNum()
- ccall('getNum') can be used to call C-functions with more complex parameters

## Notes

- Write C-code and Compile wasm in the browser: https://wasdk.github.io/WasmFiddle/
- Edit wasm: https://webassembly.studio/
- Use Webassembly Studio to edit .wat-files and insert custom memory imports as defined in JS.
- Note that a lot of code in the project is actually created by Emscripten on compiling (index.js...)
- SDL and WebGL is part of Emscripten (WebGPU in the future) and can be used to render instead of relying on Javascript.
