const express = require('express');
const app = express();
const open = require('open');
const port = 2222;

app.use(express.static('public'));
// Start server
app.listen(port, () => {
  console.log(`Server runnning on port ${port}`);
  open(`http://localhost:${port}`);
});